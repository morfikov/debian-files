#
# also see .oh-my-zsh/lib/history.zsh
#

HISTSIZE=500000
SAVEHIST=100000

# avoid "beep"ing
setopt nobeep


#unsetopt extended_history       # record timestamp of command in HISTFILE
#unsetopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
#unsetopt hist_ignore_dups       # ignore duplicated commands history list
#unsetopt hist_verify            # show command with history expansion to user before running it
#unsetopt inc_append_history     # add commands to HISTFILE in order of execution
#unsetopt share_history          # share command history data

#setopt append_history           # append history
#setopt hist_ignore_space        # ignore commands that start with space
#setopt hist_ignore_all_dups     # ignore duplicates in history
#setopt hist_no_functions        # do not save functions in history
setopt hist_no_store            # do not save history command in history

# Restore default shell behavior
bindkey '^[l' down-case-word
bindkey '^U' backward-kill-line
